import './App.css';
import Button from './components/Button/button';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Button label="Belajar React JS"/>
      </header>
    </div>
  );
}

export default App;
